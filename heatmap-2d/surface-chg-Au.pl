#!/usr/bin/perl

use strict;
use warnings;
use Math::Trig;

my $capac = 1.2159; # capacitance in a.u.

my (@x, @y, @z, @name, @res, @resnr);
my ($i, @charge);
my $Bohr2AA = 0.52917721092;

my $n_gold = 0;
open OUT, "data.out" or die;
while (<OUT>)
{
    if (/Nanoparticle  1/)
    {
        $_ = <OUT>;
        $_ = <OUT>;
        $_ = <OUT>;
        while (1)
        {
            $_ = <OUT>;
            last if (/^\s*====/);
            $n_gold ++;
            @_ = split;
            $name[$n_gold] = "Au";
            $res[$n_gold] = "AU";
            $resnr[$n_gold] = 1;
            $x[$n_gold] = $_[1] * $Bohr2AA;
            $y[$n_gold] = $_[2] * $Bohr2AA;
            $z[$n_gold] = $_[3] * $Bohr2AA;
        }
    }
    if (/Computed MQ vector/)
    {
        $_ = <OUT>;
        $_ = <OUT>;
        for $i ( 1 .. $n_gold*3 )
        {
            $_ = <OUT>;
        }
        for $i ( 1 .. $n_gold )
        {
            $_ = <OUT>;
            @_ = split;
            die unless (($_[0]-$n_gold*3) == $i);
            $charge[$i] = $_[1];
        }
    }
}
close OUT;

my $qmax = -1e10;
my $qmin = 1e10;
for $i ( 1 .. $n_gold )
{
    $qmax = $charge[$i] if $charge[$i] > $qmax;
    $qmin = $charge[$i] if $charge[$i] < $qmin;
}
printf "Max = %.6f\n", $qmax;
printf "Min = %.6f\n", $qmin;
printf "Mid = %.6f\n", abs($qmin)/($qmax-$qmin);

# get x/y boundaries

my ($xmin, $xmax) = (1e10, -1e10);
my ($ymin, $ymax) = (1e10, -1e10);
for $i ( 1 .. $n_gold )
{
    if    ($x[$i] < $xmin) { $xmin = $x[$i]; }
    elsif ($x[$i] > $xmax) { $xmax = $x[$i]; }
    if    ($y[$i] < $ymin) { $ymin = $y[$i]; }
    elsif ($y[$i] > $ymax) { $ymax = $y[$i]; }
}
$xmin -= 2.5;
$xmax += 2.5;
$ymin -= 2.5;
$ymax += 2.5;

# get 2d Gaussian distribution of charges

my ($dx, $dy) = (0.1, 0.1);

my $R = sqrt(2.0) * (sqrt(2.0 / pi) * $capac) * $Bohr2AA;
my $pi32R3 = 1.0 / (pi**1.5 * $R**3);

my $nx = sprintf "%.0f", 1.0 / $dx * ($xmax - $xmin);
my $ny = sprintf "%.0f", 1.0 / $dy * ($ymax - $ymin);

printf "nx = %d\n", $nx+1;
printf "ny = %d\n", $ny+1;
printf "X_max = %f\n", $xmax;
printf "X_min = %f\n", $xmin;
printf "Y_max = %f\n", $ymax;
printf "Y_min = %f\n", $ymin;

my ($dens_min, $dens_max) = (1e10, -1e10);

open TXT,">data.txt" or die;
my ($ix, $iy);
for $ix ( 0 .. $nx )
{
    for $iy ( 0 .. $ny )
    {
        my $dens = 0.0;
        for $i ( 1 .. $n_gold )
        {
            # only the first layer of gold atoms
            next if $z[$i] < -2.0;

            # Gaussian distributed charge
            # Mayer, PRB, 75, 045407 (2007)
            my $dr2 = (($ix+0.5)*$dx+$xmin - $x[$i])**2 +
                      (($iy+0.5)*$dy+$ymin - $y[$i])**2;
            $dens += $charge[$i] * $pi32R3 * exp(-$dr2 / $R**2);
        }
        printf TXT "%f %f %20.10e\n", ($ix+0.5)*$dx+$xmin, ($iy+0.5)*$dy+$ymin, $dens;

        # upper and lower bound of charge densities
        if    ($dens > $dens_max) { $dens_max = $dens; }
        elsif ($dens < $dens_min) { $dens_min = $dens; }
    }
    printf TXT "\n";
}

printf "Dens_max = %20.10e\n", $dens_max;
printf "Dens_min = %20.10e\n", $dens_min;

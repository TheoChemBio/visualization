Usage:

```
perl surface-chg-Au.pl
gnuplot plot.gnu > plot.png
```

[https://pubs.acs.org/doi/abs/10.1021/ct500579n](https://pubs.acs.org/doi/abs/10.1021/ct500579n)

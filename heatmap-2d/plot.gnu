#!/usr/bin/gnuplot

set terminal png enhanced font Helvetica 100 size 3500,3000

unset key

set size square
set multiplot
set view map

set border linewidth 5

set xlabel font "Helvetica 60"
set xlabel "x / Angstrom"
set xrange [-17.5:17.5]
set xtics 5
set ylabel font "Helvetica 60"
set ylabel "y / Angstrom"
set yrange [-17.5:17.5]
set ytics 5
set cbrange [-1.0e-2:1.0e-2]
set cbtics 0.5e-2
#set format cb "%.1tx10^{%T}"
set format cb "%.3f"
set palette defined ( 0 "#000088", 15 "#0000ff", 35 "#00ffff", 50 "#ffffff", 60 "#ffff00", 70 "#ffa500", 85 "#ff0000", 100 "#880000" )
splot "data.txt" u 1:2:3 w image
unset surf

## VMD script for cube file

This example shows VMD rendering for cube files.

+ Usage

    Update VMD and Tachyon command in ``run_vmd.sh``, and then run the script.
    It will process all .cube files in the current folder.

    ```
    bash run_vmd.sh
    ```


# Visualization

The purpose of this repository is to host some visualization scirpts and tools
for molecules and/or orbitals.

+ VMD script for cube file

    Visualizes cube file as isosurfaces.

+ VMD script for xyz file

    Visualizes xyz file in licorice mode.


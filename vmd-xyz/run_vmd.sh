#!/bin/bash

set -u
set -e

# vmd and tachyon command

vmd_bin="/Applications/VMD\ 1.9.2.app/Contents/MacOS/startup.command"
tachyon="/Applications/VMD\ 1.9.2.app/Contents/vmd/tachyon_MACOSXX86"

# process all xyz files

files=$(ls *.xyz)

for file in $files; do

    name=${file%.xyz}
    echo $name

    # create vmd visualization state file

    sed -e "s/_ref_/$name/g" ref.vmd > $name.vmd

    echo "display resetview" >> $name.vmd
    echo "display resize 800 800" >> $name.vmd
    echo "display antialias on" >> $name.vmd
    echo "display ambientocclusion on" >> $name.vmd
    echo "display shadows on" >> $name.vmd
    echo "scale by 1.6" >> $name.vmd
    echo "axes location off" >> $name.vmd
    echo "render Tachyon $name" >> $name.vmd
    echo "$tachyon -aasamples 36 $name -format TARGA -o $name.tga" >> $name.vmd
    echo "exit" >> $name.vmd

    # use eval to take care of space in vmd command

    eval $vmd_bin -dispdev text -e $name.vmd
    convert -compress lzw $name.tga $name.tiff

    # trash files without using rm

    if [ -f $name.tiff ]; then
        mv $name     00.tmp
        mv $name.tga 00.tmp
        echo -n "" > 00.tmp
    fi

done

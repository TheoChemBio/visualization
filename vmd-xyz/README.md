## VMD script for xyz file

This example shows VMD rendering for xyz files.

+ Usage

    Update VMD and Tachyon command in ``run_vmd.sh``, and then run the script.
    It will process all .xyz files in the current folder.

    ```
    bash run_vmd.sh
    ```


import veloxchem as vlx

# create molecule

mol_str = """
    O     0.000000    0.000000    0.000000
    H     0.000000    0.504284    0.758602
    H     0.000000   -0.504284    0.758602
"""
molecule = vlx.Molecule.read_str(mol_str, units='angstrom')

# create basis set

basis = vlx.MolecularBasis.read(molecule, 'def2-svp')

# create visualization driver

vis_drv = vlx.VisualizationDriver()

atom_to_ao = vis_drv.map_atom_to_atomic_orbitals(molecule, basis)
print('Number of atoms:', len(atom_to_ao))
print('Atom to AO mapping:')
print(atom_to_ao)

# get atomic orbital (AO) information, including:
# 1) atomic number of the atom
# 2) angular momentum of the AO
# 3) spherical harmonic index of the AO
# 4) basis function index of the AO
ao_info = vis_drv.get_atomic_orbital_info(molecule, basis)

print('Number of AOs:', len(ao_info))
print('AO information:')
print(ao_info)

print('Number of unique AOs:', len(set([tuple(ao) for ao in ao_info])))

# create cubic grid

cube_origin = (-0.1, -0.1, -0.1)
cube_stepsize = (0.1, 0.1, 0.1)
cube_points = (3, 3, 3)
grid = vlx.CubicGrid(cube_origin, cube_stepsize, cube_points)

vis_drv.compute_atomic_orbital_for_grid(grid, basis, ao_info[2])

print('Values at cubic grid points:')
print(grid.values_to_numpy())
